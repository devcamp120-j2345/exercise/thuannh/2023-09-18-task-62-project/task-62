package com.devcamp.project.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.project.models.District;
import com.devcamp.project.models.Province;
import com.devcamp.project.models.Ward;
import com.devcamp.project.repository.DistrictRepository;
import com.devcamp.project.repository.ProvinceRepository;
import com.devcamp.project.repository.WardRepository;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ProvinceController {
    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private WardRepository wardRepository;

    @GetMapping("provinces")
    public ResponseEntity<Page<Province>> getProvince(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size) {
        try {
            PageRequest pageRequest = PageRequest.of(page, size);
            Page<Province> provincePage = provinceRepository.findAll(pageRequest);
            return new ResponseEntity<>(provincePage, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts/{province_id}")
    public ResponseEntity<ArrayList<District>> getDistrictsByProvince(@PathVariable int province_id) {
        try {
            ArrayList<District> districts = new ArrayList<>();
            districtRepository.findByProvinceId(province_id).forEach(districts::add);
            return new ResponseEntity<>(districts, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/wards/{district_id}")
    public ResponseEntity<ArrayList<Ward>> getWardssByDistrict(@PathVariable int district_id) {
        try {
            ArrayList<Ward> wards = new ArrayList<>();
            wardRepository.findByDistrictId(district_id).forEach(wards::add);
            return new ResponseEntity<>(wards, HttpStatus.OK);
        }catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
