package com.devcamp.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.project.models.Province;

public interface ProvinceRepository extends JpaRepository<Province, Long>{
    
}
