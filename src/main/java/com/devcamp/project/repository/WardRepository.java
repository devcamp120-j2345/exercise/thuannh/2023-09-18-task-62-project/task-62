package com.devcamp.project.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.project.models.Ward;

public interface WardRepository extends JpaRepository<Ward, Long> {
    ArrayList<Ward> findByDistrictId(int id);
}
