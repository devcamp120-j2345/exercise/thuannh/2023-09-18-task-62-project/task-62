package com.devcamp.project.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.project.models.District;

public interface DistrictRepository extends JpaRepository<District, Long> {
    ArrayList<District> findByProvinceId(int id);
}
